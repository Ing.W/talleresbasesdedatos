CREATE DATABASE  IF NOT EXISTS `medicina` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `medicina`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: medicina
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `medical_history`
--

DROP TABLE IF EXISTS `medical_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medical_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `eps` varchar(45) NOT NULL,
  `ips` varchar(45) NOT NULL,
  `patients_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idHistoriaClinical_UNIQUE` (`id`),
  KEY `paciente_fk_idx` (`patients_id`),
  CONSTRAINT `paciente_fk` FOREIGN KEY (`patients_id`) REFERENCES `patients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_history`
--

LOCK TABLES `medical_history` WRITE;
/*!40000 ALTER TABLE `medical_history` DISABLE KEYS */;
INSERT INTO `medical_history` VALUES (1,'emssanar','pasto salud',2),(2,'sanitas','san pedro',3),(3,'coomeva','ips coomeva',4),(4,'nueva','ips nueva',5),(5,'famisanar','ips avenida',6),(6,'emssanar','iphg',7),(7,'nueva','sin eps',8);
/*!40000 ALTER TABLE `medical_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical_visit`
--

DROP TABLE IF EXISTS `medical_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medical_visit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `clinic_history_id` int NOT NULL,
  `date` date NOT NULL,
  `diagnosis` varchar(45) NOT NULL,
  `symptom` varchar(45) NOT NULL,
  `value` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`clinic_history_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `historia clinica fk` FOREIGN KEY (`clinic_history_id`) REFERENCES `medical_history` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical_visit`
--

LOCK TABLES `medical_visit` WRITE;
/*!40000 ALTER TABLE `medical_visit` DISABLE KEYS */;
INSERT INTO `medical_visit` VALUES (1,1,'2022-12-31','mareo','diarrea',1000),(2,2,'2022-01-01','vetigo','cansancio',5000),(3,3,'2022-03-07','cancer','mareo',6),(4,4,'2022-04-06','deshidratacion','resequedad',1500),(5,5,'2022-05-05','ezquisofrenia','desorintacion',4),(6,6,'2022-12-31','asdasdasd','asdasdasd',5000),(7,7,'2022-09-03','peritonitis','malestar',6000);
/*!40000 ALTER TABLE `medical_visit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `identificacion_type` varchar(45) NOT NULL,
  `identification_number` varchar(45) NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `identification_name_UNIQUE` (`identification_number`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (2,'Juan','Fernando','CC','1080521457',1),(3,'Maria','Camila','TI','1085457894',1),(4,'Jorge','Armando','CE','152584784',0),(5,'Hector','Adalberto','TI','1050245877',1),(6,'Juan','Jose','TI','15785457',0),(7,'fabio','perez','CC','456456456',1),(8,'paciente sin eps','sin apellido','cc','123123123',0);
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-12 14:25:03
